Source: ros2-ament-cmake-ros
Section: devel
Priority: optional
Maintainer: Debian Robotics Team <team+robotics@tracker.debian.org>
Uploaders:
 Timon Engelke <debian@timonengelke.de>,
 Jochen Sprickerhof <jspricke@debian.org>,
 Timo Röhling <roehling@debian.org>,
Build-Depends:
 ament-cmake,
 ament-lint (>= 0.13.1-2~),
 cmake,
 debhelper-compat (= 13),
 dh-ros,
 dh-sequence-python3,
 python3-all,
 python3-ament-package,
 python3-pytest <!nocheck>,
 python3-setuptools,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/ros2/ament_cmake_ros
Vcs-Git: https://salsa.debian.org/robotics-team/ros2-ament-cmake-ros.git
Vcs-Browser: https://salsa.debian.org/robotics-team/ros2-ament-cmake-ros

Package: ament-cmake-ros
Architecture: all
Multi-Arch: foreign
Depends:
 ament-cmake,
 ament-cmake-googletest:any,
 ament-cmake-pytest,
 python3-domain-coordinator,
 ${misc:Depends},
Description: CMake build system for ROS 2 ament packages (ROS extension)
 The ament build system is the most common way to build packages for ROS 2.
 ament_cmake provides the necessary tooling to build ament packages with
 CMake.
 .
 This package provides ROS specific CMake helper functions, in particular
 a way to run tests in an isolated DDS network domain.

Package: python3-domain-coordinator
Architecture: all
Multi-Arch: foreign
Section: python
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: ROS 2 test domain coordinator
 This package is part of ROS 2, the Robot Operating System.
 It provides the means to isolate concurrently running ROS 2
 test suites by providing locally unique ROS_DOMAIN_IDs.
